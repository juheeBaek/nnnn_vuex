import Vue from 'vue';
import VueRouter from 'vue-router';
import TeamView from '../views/TeamView.vue';
import ReportView from '../views/ReportView.vue';
import RankView from '../views/RankView.vue';
import MemberView from '../views/MemberView.vue';
import bus from '../utils/bus.js';
import { store } from '../store/index.js';

Vue.use(VueRouter);

export const router = new VueRouter({

    mode: 'history',
    routes: [
    {
      path: '/',
      redirect: '/team'
    },
    {
      path: '/team',
      name: 'team',
      // component: createListView('NewsView'),
      component: TeamView,
    },
    {
      path: '/team',
      name: 'team',
      // component: createListView('NewsView'),
      component: TeamView,
    },
    {
      path: '/report',
      name: 'report',
      // component: createListView('NewsView'),
      component: ReportView,
    },
    {
      path: '/rank',
      name: 'rank',
      // component: createListView('NewsView'),
      component: RankView,
    },
    {
      path: '/member',
      name: 'member',
      // component: createListView('NewsView'),
      component: MemberView,
    }
   /* {
        path: '/ask',
        name: 'ask',
        // component: createListView('AskView'),
        component: AskView,
        beforeEnter: (to, from, next) => {
            bus.$emit('start:spinner');
            store.dispatch('FETCH_LIST', to.name)
                .then(() => {
                    // bus.$emit('end:spinner');
                    next();
                })
                .catch((error) => {
                    console.log(error);
                });
        },
    },
    {
        path: '/jobs',
        name: 'jobs',
        // component: createListView('JobsView'),
        component: JobsView,
        beforeEnter: (to, from, next) => {
            bus.$emit('start:spinner');
            store.dispatch('FETCH_LIST', to.name)
                .then(() => {
                    next();
                })
                .catch((error) => {
                    console.log(error);
                });
        },
    },
    {
      path: '/item/:id',
      component: ItemView,
    },
    {
      path: '/user/:id',
      component: UserView,
    }*/
  ]
})