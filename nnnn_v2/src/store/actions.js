/*import {
    fetchNewsList,
    fetchAskList,
    fetchJobsList,
    fetchUserInfo,
    fetchItem,
    fetchList
} from '../api/index.js';*/

export default {
    // promise
    // FETCH_NEWS ({ commit }) {
    //     return fetchNewsList().then(response => commit('SET_NEWS', response.data));
    // },
    // async

    async FETCH_NEWS({ commit }) {
        try {
            const response = await fetchNewsList();
            commit('SET_NEWS', response.data);
            return response;
        }catch(error) {
            console.log(error);
        }
    },

    async FETCH_ASK({ commit }) {
        const response = await fetchAskList();
        commit('SET_ASK', response.data);
        return response;
    },

    async FETCH_JOBS({ commit }) {
        try {
            const response = await fetchJobsList();
            commit('SET_JOBS', response.data);
            return response;
        }catch(error) {
            console.log(error);
        }

    },

    async FETCH_USER({ commit }, userId) {
        const res = await fetchUserInfo(userId);
        commit('SET_USER', res.data);
        return res;
        // promise
        // return fetchUserInfo(userId).then(res => commit('SET_USER', res.data));
    },

    async FETCH_ITEM({ commit }, itemId) {
        const res = await fetchItem(itemId);
        commit('SET_ITEM', res.data);
        return res;
    },
    // hoc
    // #2
    async FETCH_LIST({ commit }, pageName) {
        // #3
        const response = await fetchList(pageName);
        // #4
        console.log(4);
        commit('SET_LIST', response.data);
        return response;

    },
}