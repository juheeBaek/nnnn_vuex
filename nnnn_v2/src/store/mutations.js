export default {
    SET_NEWS(state, news_data) {
        state.news = news_data;
    },
    SET_JOBS(state, jobs_data){
        state.jobs = jobs_data;
    },
    SET_ASK(state, ask_data){
        state.ask = ask_data;
    },
    SET_USER(state, user){
        state.user = user;
    },
    SET_ITEM(state, item){
        state.item = item;
    },
    SET_LIST(state, list){
        state.list = list;
    }
}